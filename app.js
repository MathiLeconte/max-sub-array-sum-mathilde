console.log("Loading samples ...");
var samples = require('./samples/samples.js');
console.log("OK");
console.log("");

function maxSubArraySum(tab) {
    if (tab.length == 0){
        console.error("tab don't have any items, max sub array sum cannot be calculated")
    }
    //var maxSubArraySum = maxSubArraySumFirstway(tab) 
    //-> first algorythm self made
    //var maxSubArraySum = maxSubArraySumKadane(tab)
    // -> second algorythm find here: 
    var maxSubArraySum = maxSubArraySumKadane(tab)
    return maxSubArraySum;
}

function maxSubArraySumFirstway(tab) {
    var maxSubArraySum = Math.max.apply(null, tab);
    for (let i = 0; i < tab.length; i++) {
        if (tab[i]>0) {
            for (let j = i+1; j < tab.length+1; j++) {
                if (tab[j]>0) { 
                    var subsum =subArraySum(tab, i, j)
                    if(subsum>maxSubArraySum){
                        maxSubArraySum = subsum
                    }
                }
            }
        }
    }
    return maxSubArraySum;
}

function subArraySum(tab, i, j) {
    var sum = 0;
    var subtab = tab.slice(i,j+1);
    subtab.forEach(element => {
        sum +=element;
    });
    return sum;
}

function maxSubArraySumKadane(tab) {
    var localsubsum = tab[0];
    var maxSubArraySum = tab[0];

    for (let i = 1; i < tab.length+1; i++) {
        var localsubsum = Math.max(localsubsum+tab[i],tab[i]);
        if(maxSubArraySum<=localsubsum){
            maxSubArraySum = localsubsum;
        }
    }
    return maxSubArraySum;
}

function test(sample) {
    console.log("== Test maxSubArraySum on a " + sample.size + " array == ")
    var start = new Date().getTime();
    var result = maxSubArraySum(sample.tab);
    var end = new Date().getTime();
    console.log("result => " + result);
    console.log("expected => " + sample.result);
    if (result !== sample.result) {
        console.log("Error : Bad result on following tab :"+sample.size);
        console.log("Error : Bad result on following tab :"+sample.size);
        //console.log(JSON.stringify(sample.tab));
    } else console.log("Excution time : " + (end - start) + "ms");

    console.log("");
}

function main() {
    // mode = "dev" // samples array from 1 to 10 elements
    // mode = "test" // samples array from 1 to 5 000 elements
    // mode = "huge" // samples array from 1 to 10 000 000 elements
    var mode = "huge";
    var samplesToTest = samples[mode];
    for (var i = 0; i < samplesToTest.length; i++) {
        test(samplesToTest[i]);
    }
}

main();